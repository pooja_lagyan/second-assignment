package com.customerrecord.util;
import com.customerrecord.entities.*;
import java.util.Scanner;

public class AddCustomerClass implements Constants 
{    
    public static int autoIncrementCustomerId = 1;
    public Customer addNewCustomer()
    {
      Scanner obj = new Scanner(System.in);
      Customer customerObj = new Customer();
      int typeOfCar,choiceCar=0;
      try
      {
        customerObj.setCustomerId(autoIncrementCustomerId++);
        System.out.print("Enter Customer Name : ");
        customerObj.setCustomerName(obj.nextLine());
        //for every Customer carID is initialised from 1 
        customerObj.carId = 1;
        LabelBreak :
        while(choiceCar==0)
        {
            System.out.print("Enter type of car \n");
            System.out.print("1. Maruti \t2.Toyota \t3.Hyundai : ");
            typeOfCar=obj.nextInt();
            Car carObj = null;
            switch(typeOfCar)
            {
                case MARUTI :
                    carObj = new Maruti();
                    carObj.typeOfCar = "Maruti";
                    break;
                
                case TOYOTA :
                    carObj = new Hyundai();
                    carObj.typeOfCar = "Toyota";
                    break;
                
                case HYUNDAI :
                    carObj = new Toyota();
                    carObj.typeOfCar = "Hyundai";
                    break;
                
                default :
                    System.out.println("wrong choice");
                    break LabelBreak;
            }          
            obj.nextLine();
            carObj.carId= customerObj.carId++;
            System.out.print("Enter the Model of Car : ");
            carObj.setCarModel(obj.nextLine());
            System.out.print("Enter the Price of Car : ");
            carObj.setPrice(obj.nextFloat());
            carObj.setResaleValue();
            customerObj.cars.add(carObj);  
            System.out.println("Want to enter more car ? (0-yes/1-no)");
            choiceCar=obj.nextInt();
            obj.nextLine();
        }
      }
      catch(Exception e)
      {
               System.out.println(e+" -- Exception Found ");
      }
      return customerObj;
  }    
}
