package com.customerrecord.util;
import com.customerrecord.entities.*;
import java.util.Scanner;
import java.util.Iterator;
import java.util.LinkedList;

public class AddCarClass implements Constants {

    public void addNewCar(LinkedList<Customer> list) 
    {
        Scanner obj = new Scanner(System.in);
        int typeOfCar, id;
        boolean found = false;
        try 
        {
           Iterator itr = list.iterator();
           Customer customerObj = null;
            //As ids of customers are auto generated so we need to show ids to admin 
            //Iterator itr is used for printing Customer ids and name
           
           if(itr.hasNext())
           {
                System.out.println("Ids of Customers are : ");
                while(itr.hasNext())
                {
                    customerObj = (Customer) itr.next();
                    System.out.println(customerObj.customerId+"-"+customerObj.customerName+" ");
                }
                System.out.println("Choose the ID of customer : ");
                id = obj.nextInt();
                Iterator itr2 = list.iterator();
                //itr2 is used for finding the id in the customer list
            
                while (itr2.hasNext()) 
                {
                    customerObj = (Customer) itr2.next();
                    if (id == customerObj.customerId) {
                        found = true;
                        break;
                }
            }
            // If user enters wrong choice for the type of car then default case will be executed
            // So it will come out of switch loop but not else loop
            // That's why label is used to break the if else loop
            LabelBreak:
            if (found == false) {
                System.out.println("Id not found");
            } 
            else 
            {
                Car carObj = null;
                System.out.print("Enter type of car \n");
                System.out.print("1. Maruti \t2.Toyota \t3.Hyundai : ");
                typeOfCar = obj.nextInt();
                switch (typeOfCar) 
                {
                    case MARUTI:
                        carObj = new Maruti();
                        carObj.typeOfCar = "Maruti" ;
                        break;

                    case TOYOTA:
                        carObj = new Toyota();
                        carObj.typeOfCar = "Toyota";
                        break;

                    case HYUNDAI:
                        carObj = new Hyundai();
                        carObj.typeOfCar = "Hyundai";
                        break;

                    default:
                        System.out.println("wrong choice");
                        break LabelBreak;
                }
                obj.nextLine();
                carObj.carId = customerObj.carId++;
                System.out.print("Enter the Model of Car : ");
                carObj.setCarModel(obj.nextLine());
                System.out.print("Enter the Price of Car : ");
                carObj.setPrice(obj.nextFloat());
                carObj.setResaleValue();
                customerObj.cars.add(carObj);
            }
          }
          else
          {
                System.out.print("No Customer Found");
          }        
        } 
        catch (Exception e) 
        {
            System.out.println(e + " -- Exception Found");
        }
    }
}
