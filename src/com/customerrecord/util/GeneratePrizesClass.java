package com.customerrecord.util;
import java.util.LinkedList;
import com.customerrecord.entities.*;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;

public class GeneratePrizesClass
{
 
    public void generatePrizes(LinkedList<Customer> list)
    {
        Iterator itr = list.iterator();
        Customer customerObj = null;
        
        if(itr.hasNext())
        {
            Scanner obj = new Scanner(System.in);
            Boolean loss = true;
            //idList will store all customers ids in a list 
        
            LinkedList<Integer> idList = new LinkedList<Integer>();
            while(itr.hasNext())
            {
                customerObj=(Customer)itr.next();
                idList.add(customerObj.customerId);
            }
            Random random = new Random();
            //randomList will store any 6 randomly generated ids from idList
        
            LinkedList<Integer> randomIdList = new LinkedList<Integer>();
            while(randomIdList.size()<6)
            {
                randomIdList.add(idList.get(random.nextInt(idList.size())));
            }
            // adminChoice List will Store the list of ids entered by admin
        
            LinkedList<Integer> adminChoiceList = new LinkedList<Integer>();
            Iterator idItr = list.iterator();
            Customer customerObj2 = null;
            System.out.println("Ids of Customers are : ");
            while(idItr.hasNext())
            {
                customerObj2 = (Customer) idItr.next();
                System.out.println(customerObj2.customerId+"-"+customerObj2.customerName);
            }
            
            try 
            {   
                System.out.println(" Choose any 3 random Ids : ");
                while(adminChoiceList.size()<3)
                {
                    
                    adminChoiceList.add(obj.nextInt());
                }
                
                int index = 0;
                adminChoiceList.retainAll(randomIdList);
                Iterator idAdminItr = adminChoiceList.iterator();
                
                while(idAdminItr.hasNext())
                {          
                        loss = false; 
                        System.out.print("Congratulations Customer with id "+adminChoiceList.get(index)+" won\n");
                        idAdminItr.next();
                        index++;
                }
                if(loss == true)
                {
                    System.out.println("No winner..");
                }
            }
            catch(Exception e)
            {
                System.out.println(e+" -- Exception Found..");
            }
        }
        else
        {
            System.out.println("No Customer found in the list..");
        }
    }
}
