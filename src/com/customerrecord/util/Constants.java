
package com.customerrecord.util;


public interface Constants {
    
    int ADD_CUSTOMER = 1;
    int ADD_CAR = 2;
    int LIST_ALL_CUSTOMERS = 3;
    int LIST_CUSTOMER = 4;
    int GENERATE_PRIZE = 5;
    int EXIT = 6;
    
    int MARUTI = 1;
    int TOYOTA = 2;
    int HYUNDAI = 3;
}
