package com.customerrecord.main;

import java.util.LinkedList;
import java.util.Scanner;
import com.customerrecord.util.*;
import com.customerrecord.entities.*;
        
public class CustomerRecord implements Constants{
    
    public static void main(String[] args)
    {
        Scanner obj=new Scanner(System.in);
        LinkedList<Customer> customerList =new LinkedList<Customer>();
        int choice,continueVariable = 0;
        try 
        {
            while(continueVariable == 0)
            {
                System.out.println(" ENTER ANY CHOICE ");
                System.out.println("1. Add new customer");
                System.out.println("2. Add new car to existing customer");
                System.out.println("3. List all customers with details sorted by car model name ");
                System.out.println("4. List individual customer");
                System.out.println("5. Generate Prizes");
                System.out.println("6. Exit ");
                choice=obj.nextInt();        
                switch(choice)
                {
                    case ADD_CUSTOMER :
                        AddCustomerClass addCustomerObj = new AddCustomerClass();
                        customerList.add(addCustomerObj.addNewCustomer());    
                        break;
            
                    case ADD_CAR :
                        AddCarClass addCarObj = new AddCarClass();
                        addCarObj.addNewCar(customerList);
                        break;
            
                    case LIST_ALL_CUSTOMERS :
                        ListCustomerClass listObj = new ListCustomerClass();
                        listObj.listAllCustomers(customerList);
                        break;
            
                    case LIST_CUSTOMER :
                        ListCustomerClass listOneObj = new ListCustomerClass();
                        listOneObj.listOneCustomer(customerList);
                        break;
                
                    case GENERATE_PRIZE :
                        GeneratePrizesClass generatePrizeObj = new GeneratePrizesClass();
                        generatePrizeObj.generatePrizes(customerList);
                        break;
                
                    case EXIT :
                        System.exit(0);
                        break;
                
                    default :
                        System.out.println("wrong choice");
                }
                System.out.print("\nDo you wish to continue ? (0-yes/1-no) : ");
                continueVariable = obj.nextInt();
            }    
        }
        catch(Exception e)
        {
            System.out.print(e+" - Exception Found \n");
        }
    }
}
